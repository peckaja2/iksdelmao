
# -*- coding: utf-8 -*-
from re import U, X
from PIL import Image
import cv2 as cv
import numpy as np
import os
import qi
from naoqi import ALProxy

app = qi.Application()
app.start()

session = app.session

proxy = ALProxy("ALVideoDevice", "10.10.48.223", 9559)
tts = session.service('ALTextToSpeech')
tts.setLanguage("Czech")
# Vymaze zabrane kamery z predchozich pokusu
proxy.unsubscribeAllInstances("cam")
name = "cam"
camID = 0              # 0 horni, 1 spodni
resolution = 2         # 640*480
colorSpace = 13        # format BGR
fps = 30
# Seznam konstant naleznete zde: http://doc.aldebaran.com/2-1/family/robots/video_robot.html#cameracolorspace-mt9m114
cam = proxy.subscribeCamera(name, camID, resolution, colorSpace, fps) # zabrani kamery
image = proxy.getImageRemote(cam) # porizeni snimku
im = image[6] # vytazeni bajtoveho pole
img = np.fromstring(im, np.uint8) # konverze na cisla
img = img.reshape(480,640,3) # vytvoreni dvourozmerneho pole a BGR trojic
cv.imshow("camera", img) # zobrazeni vysledneho obrazu

proxy.unsubscribe(cam) # Vymazani zabrane kamery


# try:
#     img  = cv.imread("balls.png")
# except IOError:
#     pass

hsv_img = cv.cvtColor(img, cv.COLOR_BGR2HSV)

lower_yell = np.array([20,90,50])
upper_yell = np.array([40,255,255])

lower_blue = np.array([95,100,50])
upper_blue = np.array([140,255,255])
#horni cast spektra rozsah
lower_red = np.array([160,100,50])
upper_red = np.array([180,255,255])

upper_green = np.array([70,255,255])

mask_yellow = cv.inRange(hsv_img, lower_yell, upper_yell)
mask_red1 = cv.inRange(hsv_img, lower_red, upper_red)
mask_red2 = cv.inRange(hsv_img, np.array([0,90,50]), np.array([10,255,255])) #cervena spodek spektra rozsah
mask_blue = cv.inRange(hsv_img, lower_blue, upper_blue)
mask_green = cv.inRange(hsv_img, lower_green, upper_green)
mask_red = cv.bitwise_or(mask_red1, mask_red2)

print(mask_yellow.shape)

#res_red = cv.bitwise_and(img,img, mask= cv.bitwise_or(mask_red1, mask_red2))
#res_yellow = cv.bitwise_and(img, img, mask = mask_yellow)
#res_blue = cv.bitwise_and(img, img, mask = mask_blue)
#res_green = cv.bitwise_and(img, img, mask = mask_green)

mask_all = cv.bitwise_or(cv.bitwise_or(cv.bitwise_or(mask_blue, mask_yellow), cv.bitwise_or(mask_red1, mask_red2)), mask_green)



res = cv.bitwise_and(img, img, mask = mask_all)

# blurImg = cv.GaussianBlur(res,(3, 3), 0)
grayImg = cv.cvtColor(img, cv.COLOR_BGR2GRAY)

# cv.imshow("test", grayImg)

# cervena je param1 = 70 param2=20, zluta tez
# circles = cv.HoughCircles(  grayImg, cv.HOUGH_GRADIENT, 1, 50,
#                             param1=70,param2=20,minRadius=0,maxRadius=50)

params = cv.SimpleBlobDetector_Params()

params.filterByArea = True
params.minArea = 50
params.maxArea = 500000
params.filterByCircularity = True
params.minCircularity = 0.5

median = cv.medianBlur(mask_all, 5)

mask_all = cv.bitwise_not(median)
cv.imshow("mask", mask_all)

detector = cv.SimpleBlobDetector_create(params)
circles = detector.detect(mask_all)
print(circles)

print("Pocet micku: ", len(circles))
tts.say("Vidím "+ str( len(circles))+ "míčky.")

for cir in circles:
    x = cir.pt[1]
    y = cir.pt[0]
    print("mic: ", x, y)
    if mask_yellow[int(x)][int(y)]:
        if y <= 320 : tts.say("Zluty vlevo")
        else: tts.say("Zluty vpravo")
    if mask_red[int(x)][int(y)]:
        if y <= 320 : tts.say("Cervena vlevo")
        else: tts.say("Cervena vpravo")
    if mask_blue[int(x)][int(y)]:
        if y <= 320 : tts.say("Modra vlevo")
        else: tts.say("Modra vpravo")
    if mask_green[int(x)][int(y)]:
        if y <= 320 : tts.say("Zelena vlevo")
        else: tts.say("Zelena vpravo")

blank = np.zeros((1, 1))
blobs = cv.drawKeypoints(img, circles, blank, (0, 0, 255),cv.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)

# Získání pozice hlavy

# memory = naoqi.ALProxy("ALMemory", "10.10.48.252", 9559)
# head_position = memory.getListData([
#   "Device/SubDeviceList/HeadYaw/Position/Sensor/Value",
#   "Device/SubDeviceList/HeadPitch/Position/Sensor/Value",
# ])
# print(head_position)

distanceFromCam(60, 55, 45, circles)

cv.imshow("image", img)
cv.imshow("blobs", blobs)


#cv.imwrite("test6.png", img)

cv.waitKey(0)
cv.destroyAllWindows()



