# -*- coding: utf-8 -*-
from cgi import print_arguments
from cmath import tan
from email.header import Header
from re import U, X
from turtle import color
from PIL import Image
import cv2 as cv
from cv2 import imshow
import numpy as np
from numpy import take

file = "balls.png"
file = "test9.jpg"
file = "test2.png"

frame_width = 640
frame_heigh = 480

robot_ip = "10.10.48.222"

import qi
from naoqi import ALProxy
import naoqi

app = qi.Application()
app.start()

session = app.session

tts = session.service('ALTextToSpeech')
tts.setLanguage("Czech")

def takeImage():
    
    posture = naoqi.ALProxy("ALRobotPosture", robot_ip, 9559)
    posture.getPostureList()  # ['Crouch', 'LyingBack', 'LyingBelly', 'Sit', 'SitOnChair', 'SitRelax', 'Stand', 'StandInit', 'StandZero']
    posture.goToPosture("Stand", 1)

    motion = naoqi.ALProxy("ALMotion", robot_ip, 9559)

   # motion.changeAngles("HeadPitch", 0.6, 0.1)

    proxy = ALProxy("ALVideoDevice", robot_ip, 9559)

    # Vymaze zabrane kamery z predchozich pokusu
    proxy.unsubscribeAllInstances("cam")
    name = "cam"
    camID = 1              # 0 horni, 1 spodni
    resolution = 2         # 640*480
    colorSpace = 13        # format BGR
    fps = 30
    # Seznam konstant naleznete zde: http://doc.aldebaran.com/2-1/family/robots/video_robot.html#cameracolorspace-mt9m114
    cam = proxy.subscribeCamera(name, camID, resolution, colorSpace, fps) # zabrani kamery
    image = proxy.getImageRemote(cam) # porizeni snimku
    im = image[6] # vytazeni bajtoveho pole
    img = np.fromstring(im, np.uint8) # konverze na cisla
    img = img.reshape(480,640,3) # vytvoreni dvourozmerneho pole a BGR trojic
    cv.imshow("camera", img) # zobrazeni vysledneho obrazu

    proxy.unsubscribe(cam) # Vymazani zabrane kamery
    return img


class Ball:
    def __init__(self, x, y, d, color):
        self.x = int(x)
        self.y = int(y)
        self.r = int(d/2)
        self.color = color
        self.distance = 0
	self.horizontalAngle = 0
        self.p = "přede mnou"
        if x < frame_width / 3 :
            self.p = "nalevo"
        if x > 2* frame_width / 3 :
            self.p = "napravo"

balls = []

def findBall(mask, color):
    params = cv.SimpleBlobDetector_Params()
    params.filterByArea = True
    params.minArea = 50
    params.maxArea = 500000
    params.filterByCircularity = True
    params.minCircularity = 0.3

    mask = cv.bitwise_not(mask)
    median = cv.medianBlur(mask, 5)

    detector = cv.SimpleBlobDetector_create(params)
    circles = detector.detect(median)


    for cir in circles:
        balls.append(Ball(cir.pt[0], cir.pt[1], cir.size, color))
    cv.imshow(color, median)

def ballDistance(cameraAngle, height, cameraVFOV):
    
    bottomAngle = float(cameraAngle) + float(cameraVFOV)/float(2)
    degP = float(cameraVFOV) / float(frame_heigh)

    for ball in balls:
        realAngle = float(bottomAngle) - ( float(ball.y + ball.r) * float(degP) )
        ball.distance = int(np.tan( np.deg2rad(realAngle) ) * height )

def ballHAngle(cameraHFOV):
    
    leftAngle = - float(cameraHFOV)/float(2)
    degP = float(cameraHFOV) / float(frame_width)

    for ball in balls:
        realAngle = float(leftAngle) + ( float(ball.x) * float(degP) )
        ball.horizontalAngle = realAngle


try:
    
    img = takeImage()
   # img  = cv.imread(file)

    hsv_img = cv.cvtColor(img, cv.COLOR_BGR2HSV)
    (frame_heigh, frame_width, tmp) = img.shape
    
except IOError:
    pass

from naoqi import ALProxy
motion = ALProxy("ALMotion", robot_ip, 9559)

mask_yellow = cv.inRange(hsv_img, np.array([20,90,80]), np.array([40,255,255]))
mask_blue = cv.inRange(hsv_img, np.array([95,100,80]), np.array([140,255,255]))
mask_green = cv.inRange(hsv_img, np.array([40,100,10]), np.array([75,255,255]))
mask_red = cv.bitwise_or(cv.inRange(hsv_img, np.array([160,100,50]), np.array([180,255,255])),cv.inRange(hsv_img, np.array([0,90,50]), np.array([10,255,255])) )

findBall(mask_yellow, "Žlutý")
findBall(mask_blue, "Modrý")
findBall(mask_green, "Zelený")
findBall(mask_red, "Červený")

memory = naoqi.ALProxy("ALMemory", robot_ip, 9559)
head_position = memory.getListData([
  "Device/SubDeviceList/HeadPitch/Position/Sensor/Value",
])
print(head_position)

# ballDistance(45, 48, 43)

ballDistance(59.44, 49, 43.7)
ballHAngle(56.3)

m = " míček"
if len(balls) >= 2 : m = " míčky"
if len(balls) >= 5 : m = " míčků"
text = "Vidím " + str(len(balls)) + m

print(text)
tts.say(text)

for ball in balls:
    cv.circle(img, (ball.x, ball.y), ball.r, (0, 255, 0), 2)
    cv.circle(img, (ball.x, ball.y), 2, (0, 0, 255), 3)
    cv.putText(img, str(ball.distance), (ball.x + 30, ball.y), cv.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 0), 1, cv.LINE_AA)
    text = ball.color + " míček je " + ball.p + ", přibližně " + str(ball.distance) + " centimetrů daleko ode mě"
    print(text + "                                        " + str(ball.horizontalAngle))
    tts.say(text)
    motion.moveTo(0, 0, -np.deg2rad(ball.horizontalAngle))
    motion.walkTo(float(ball.distance)/100, 0, 0)
    break

cv.imshow("image", img)

cv.waitKey(0)
cv.destroyAllWindows()
