# -*- coding: utf-8 -*-

import qi
import naoqi
import numpy as np
import cv2
import time

app = qi.Application()
app.start()

session = app.session
s = qi.Session()
s.connect("tcp://10.10.48.91:9999")

vd =  session.service("ALVideoDevice")
imdetect = s.service("ALIMDetection")

tts = naoqi.ALProxy("ALTextToSpeech", "10.10.48.222", 9559)
tts.setLanguage("English")  # nutné jen pokud není známo jaký jazyk je nastaven

cam = vd.subscribeCamera("cam1",1,3,13,1)
#camDepth = vd.subscribeCamera("depthcam",2,1,17,20)

rimg = vd.getImageRemote(cam)
#depthimg = vd.getImageRemote(camDepth)

res = imdetect.detect(rimg, None)

vd.unsubscribe(cam)
im = rimg[6]
img = np.frombuffer(im, np.uint8)
img = img.reshape(960,1280,3) # vytvoreni dvourozmerneho pole a BGR trojic

#['Furniture', '/m/0c_jw', 0.012653762474656105, [410L, 2L, 480L, 153L], None]

i = 0

for r in res:
	print(r[0] + "   " + str(r[2]))
	if i < 3:
		cv2.rectangle(img, (r[3][1], r[3][0]), (r[3][3], r[3][2]), (0, 255, 0), 3)
		cv2.putText(img, r[0], (r[3][1] + 15, r[3][0] + 20), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 0), 8, cv2.LINE_AA)
		cv2.putText(img, r[0], (r[3][1] + 15, r[3][0] + 20), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 255, 255), 2, cv2.LINE_AA)
		tts.say("I see a " + r[0])
		i=i+1


cv2.imshow("img", img)

cv2.waitKey(0)
cv2.destroyAllWindows()

#vd.unsubscribe(camDepth)
